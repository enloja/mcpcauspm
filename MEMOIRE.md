Résumé
Au cours de la dernière décennie, les données créées à partir des sources comme les courriels, les tweets, les messages Facebook pour ne citer que ceux-la, ont connu une croissance phénoménale dans la façon dont les
gens interagissent avec les systèmes à travers le monde.  L’exploration et l’exploration de données à grande échelle sont une clé pour tirer des enseignements précieux de cette déluge de données[1]. Cependant, les renseignements personnels des utilisateurs peuvent être divulgués. Les renseignements personnels sur les employés et les clients peuvent être volés dans des bases de données appartenant à des organismes publics et à des entreprises, et les renseignements personnels sur l'emplacement peuvent être automatiquement obtenus par le biais de réseaux omniprésents[2]. De telles révélations de renseignements personnels peuvent mener à de graves violations de la vie privée. Ainsi le traitement des données personnelles des utilisateurs deviennent incontournables, l’anonymisation, comme moyen d’échapper à la réglementation sur les données personnelles, est de plus en plus envisagée et devient donc importante.

L’anonymisation est une technique appliquée aux données à caractère personnel afin d’empêcher leur identification de façon irréversible. La plupart des méthodes d'anonymisation récemment développées peuvent être divisées en deux types. La micro-agrégation et la perturbation. La micro-agrégation, utilisée dans k-anonymat et ses successeurs, regroupe plusieurs enregistrements de sorte qu'un attaquant ne peut pas les distinguer. La perturbation ajoute un bruit contrôlé à chaque valeur d'enregistrement pour rendre difficile pour un attaquant d'estimer la valeur originale tout en conservant autant que possible les propriétés statistiques de la base de données originale. Les méthodes d'anonymisation plus traditionnelles comprennent l'utilisation de pseudonymes, l'échange de valeurs d'enregistrements et la division de tables[3].

Même avec de telles mesures de protection, il y a toujours un risque pour que l'anonymat d'une personne soit compromis par la collecte d'informations à partir de différentes sources. En effet ces méthodes n’offrent pas une démarche dès la conception qui tient compte de la protection de l’anonymat comme étant un facteur essentiel. 

La majorité des applications demandent beaucoup trop d’informations pour le besoin et si on pouvait donner la possibilité d’enlever certaines informations inutiles, si on pouvait être moins précis tout en remplissant le besoin des applications. Pour cela il est important d’avoir un mécanisme, qui serait capable de fonctionner correctement sans être dans l’obligation d’aller chercher toute les informations précise sur une personne. De pouvoir détruire certaines contraintes ou exigences, pour conserver seulement le stricte nécessaire, tout en étant capable de fournir les mêmes services ou de garder les fonctionnalités de l'application intactes.




References 

[1] X. Zhang et al., "MRMondrian: Scalable Multidimensional Anonymisation for Big Data Privacy Preservation," in IEEE Transactions on Big Data, vol. PP, no. 99, pp. 1-1.doi: 10.1109/TBDATA.2017.2787661
[2] T. Okuno, M. Ichino, T. Kuboyama and H. Yoshiura, "Content-Based De-anonymisation of Tweets," 2011 Seventh International Conference on Intelligent Information Hiding and Multimedia Signal Processing, Dalian, 2011, pp. 53-56. doi: 10.1109/IIHMSP.2011.57

[3] H. Kataoka, Y. Ogawa, I. Echizen, T. Kuboyama and H. Yoshiura, "Effects of External Information on Anonymity and Role of Transparency with Example of Social Network De-anonymisation," 2014 Ninth International Conference on Availability, Reliability and Security, Fribourg, 2014, pp. 461-467.doi: 10.1109/ARES.2014.70


